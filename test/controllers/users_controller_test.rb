require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = users(:one)
  end

  test "should get index" do
    get users_url
    assert_response :success
  end

  test "should create user" do
    assert_difference('User.count') do
      post users_url, params: { user: { email: 'newemail@example.com', password: 'password', username: 'username' } }
    end

    assert_response 201
  end

  test "should show user" do
    get user_url(@user)
    assert_response :success
  end

  test "should update user" do
    patch user_url(@user), params: { user: { email: @user.email, password: 'newpassword', username: 'different' } }
    assert_response 200
    assert_equal User.find(@user.id).username, 'different'
  end

  test "should destroy user if correct token is passed" do
    assert_difference('User.count', -1) do
      delete user_url(@user), headers: { 'HTTP_DELETE_TOKEN': 'delete-me' }
    end

    assert_response 204
  end

  test "should not destroy user if correct token is not passed" do
    assert_no_difference('User.count', 'User.count') do
      delete user_url(@user)
    end

    assert_response 403
  end
end

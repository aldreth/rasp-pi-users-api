require 'test_helper'

class UserTest < ActiveSupport::TestCase
  setup do
    @user = users(:one)
  end

  test "username should be unique" do
    duplicate_user = User.new(
      username: @user.username,
      email: 'newemail@example.com',
      password: 'password'
    )

    assert_not duplicate_user.valid?
  end

  test "email should be unique" do
    duplicate_user = User.new(
      username: 'newusername',
      email: @user.email,
      password: 'password'
    )

    assert_not duplicate_user.valid?
  end

  test "passwords need to be at least 8 characters" do
    user = User.new(
      username: 'newusername',
      email: 'newemail@example.com',
      password: 'passwor'
    )

    assert_not user.valid?
  end
end

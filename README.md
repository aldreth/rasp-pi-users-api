## An api to CRUD users

This is a rails 5 application that uses ruby 2.3.0 and a postgresql database. It is currently hosted on [heroku](http://rasp-pi-users-api.herokuapp.com/users) - it is on a free dyno, so will take a little while to start on first hit. The `DELETE_TOKEN` value for this environment is `delete-me`.

### Dependencies

* ruby 2.3.0
* postgresql

### Install & development

* `bundle install` to install necessary gems
* `cp .env.dist .env` to set up the development environment
* `bundle exec rails db:create db:migrate` to set up the database
* `bundle exec rails db:seed` to create 200 local users
* `bundle exec rails server` to start the application

### Testing

This application has minitest controller & model tests.

`rails test` will run all the tests

### Background

I used rails 5 in api mode, even though it it still in beta, as I was interested in trying it out. It made it very straightforward to scaffold users, with very little extra set up required.

I used the rails `has_secure_password` to hash and store passwords. This is an secure and quick way to store passwords, without having to build it one's self or relay on other gems. I added validations to ensure a minimum password length of 8 characters.

A user can only be deleted with a HTTP header of DELETE_TOKEN with the correct value. The value is set using an environment variable.

I started implementing a react front end for this application. The git repository is https://bitbucket.org/aldreth/rasp-pi-users-api/ and it can also be viewed on [heroku](http://rasp-pi-users-react.herokuapp.com/)
